output "cluster" {
  value = module.config_ovirt.cluster
}

output "storagedomain" {
  value = module.config_ovirt.storagedomain
}

output "network" {
  value = module.config_ovirt.network
}

output "vnic_profile" {
  value = module.config_ovirt.vnic_profile
}

output "template" {
  value = module.template.template
}
