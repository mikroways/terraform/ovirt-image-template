output "cluster" {
  value = local.ovirt_cluster
}

output "network" {
  value = local.ovirt_network
}

output "storagedomain" {
  value = local.ovirt_storagedomain
}

output "vnic_profile" {
  value = local.ovirt_vnic_profile
}

