data "ovirt_clusters" "clusters" {
  search = {
    criteria       = "name = ${var.ovirt_cluster_name}"
    case_sensitive = false
  }
}


data "ovirt_networks" "networks" {
  search = {
    criteria       = "name = ${var.ovirt_network_name}"
    case_sensitive = false
    max            = 1
  }
}

data "ovirt_vnic_profiles" "vnic_profiles" {
    network_id = data.ovirt_networks.networks.networks.0.id
    name_regex = var.ovirt_vnic_profile_name
}

data "ovirt_storagedomains" "storagedomains" {
  search = {
    criteria       = "name = ${var.ovirt_storage_name}"
    case_sensitive = false
    max            = 1
  }
}

locals {
  ovirt_cluster           = data.ovirt_clusters.clusters.clusters.0
  ovirt_network           = data.ovirt_networks.networks.networks.0
  ovirt_storagedomain     = data.ovirt_storagedomains.storagedomains.storagedomains.0
  ovirt_vnic_profile      = data.ovirt_vnic_profiles.vnic_profiles.vnic_profiles.0
}
