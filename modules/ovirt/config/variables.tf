#variable "ovirt_url" {
#  type        = string
#  description = "OVirt Engine URL. For example https://ovirt-engine.example.net/ovirt-engine/api"
#}
#
#variable "ovirt_username" {
#  type        = string
#  description = "The name of user to access Engine API"
#}
#
#variable "ovirt_password" {
#  type        = string
#  description = "The plain password of user to access Engine API"
#}
#
#variable "ovirt_cafile" {
#  type        = string
#  description = "Path to a file containing the CA certificate for the oVirt engine API in PEM format"
#}
#
#variable "ovirt_ca_bundle" {
#  type        = string
#  description = "The CA certificate for the oVirt engine API in PEM format"
#}
#
#variable "ovirt_insecure" {
#  type        = bool
#  description = "Disable oVirt engine certificate verification"
#}


variable "ovirt_cluster_name" {
  type        = string
  description = "The Name of Cluster"
}

variable "ovirt_storage_name" {
  type        = string
  description = "The Name of Storage Domain for the template"
}

variable "ovirt_network_name" {
  type        = string
  description = "The name of Logical Network to use."
}

variable "ovirt_vnic_profile_name" {
  type        = string
  description = "The name of the vNIC profile of Logical Network."
}

