// Upload local image
resource "ovirt_image_transfer" "transfer_disk" {
  alias             = var.template_name
  source_url        = var.template_local_file_path
  storage_domain_id = var.ovirt_storage_domain_id
  sparse            = true
  timeouts {
    create = "20m"
  }
}

// Create the vm for import only when we don't have an existing template
resource "ovirt_vm" "tmp_import_vm" {
  name              = "tmpvm-for-${ovirt_image_transfer.transfer_disk.alias}"
  cluster_id        = var.ovirt_cluster_id
  auto_start        = false
  block_device {
    disk_id         = ovirt_image_transfer.transfer_disk.disk_id
    interface       = "virtio_scsi"
  }
  os {
    type            = "rhcos_x64"
  }
  nics {
    name            = "nic1"
    vnic_profile_id = var.ovirt_vnic_profile_id
  }
  timeouts {
    create          = "20m"
  }
  depends_on        = [ovirt_image_transfer.transfer_disk]
}

resource "ovirt_template" "template" {
  name              = var.template_name
  cluster_id        = var.ovirt_cluster_id
  vm_id             = ovirt_vm.tmp_import_vm.id
  timeouts {
    create          = "20m"
  }
}
