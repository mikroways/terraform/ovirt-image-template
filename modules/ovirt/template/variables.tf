variable "ovirt_cluster_id" {
  type        = string
  description = "The ID of Cluster"
}

variable "ovirt_storage_domain_id" {
  type        = string
  description = "The ID of Storage Domain"
}

variable "template_name" {
  type        = string
  description = "Name of template to create"
}

variable "template_local_file_path" {
  type        = string
  description = "Local file path of the template image file to upload"
}

variable "ovirt_vnic_profile_id" {
  type        = string
  description = "The ID of the vNIC profile of Logical Network"
}
