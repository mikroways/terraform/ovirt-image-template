variable "ovirt_url" {
  type        = string
  description = "The Engine URL"
}

variable "ovirt_username" {
  type        = string
  description = "The name of user to access Engine API"
}

variable "ovirt_password" {
  type        = string
  description = "The plain password of user to access Engine API"
}

variable "ovirt_cafile" {
  type        = string
  description = "Path to a file containing the CA certificate for the oVirt engine API in PEM format"
}

variable "ovirt_ca_bundle" {
  type        = string
  description = "The CA certificate for the oVirt engine API in PEM format"
}

variable "ovirt_insecure" {
  type        = bool
  description = "Disable oVirt engine certificate verification"
}
