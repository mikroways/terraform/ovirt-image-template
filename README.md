# Terraform upload template ovirt

Este repositorio muestra un ejemplo de como subir un template generado con
packer a ovirt. Podría ser cualquier otra imagen, pero usamos packer como
ejemplo.

## ¿Como se usa?

Se recomienda configurar las variables de ambiente de conexión a ovirt, por
ejemplo:

```
export TF_VAR_ovirt_username=admin@internal
export TF_VAR_ovirt_password=PASS
export TF_VAR_ovirt_url=https://ovirt-engine.example.net/ovirt-engine/api
export TF_VAR_ovirt_insecure=true
```

> Se recomienda utilizar el certificado del engine en vez del modo insecure

Asumiendo que la imagen está en la carpeta fuera del directorio actual, puede
observarse que en `main.tf` se referencia tal archivo.

Observar sobre todo, la configuración de ovirt, específicamente la red, cluster
mame y storagedomain. Puede usar las (outputs de terraform para verifcarlos)(tf-outputs).

Correr entonces:

```
terraform apply
```

> Si se usa una conexión wifi en la misma LAN que Ovirt, la trasnferencia de
> aproximadamente 3.5GB demora cerca de 15 minutos. Estos scripts dan timeout a
> los 20 minutos.

Una vez cargada la imagen, se crea un template. Es idempotente, por lo que no
sube el template si ya existe.

## TF outputs

Se imprime una serie de informacion del cluster a modo de ejemplo. Puede que en
un principio se quiera jugar con dichas salidas para verificar el template se
crea en el cluster adecuado, con la red adecuada y storagedomain adecuado.
