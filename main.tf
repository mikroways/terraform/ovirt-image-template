provider "ovirt" {
  url       = var.ovirt_url
  username  = var.ovirt_username
  password  = var.ovirt_password
  cafile    = var.ovirt_cafile
  ca_bundle = var.ovirt_ca_bundle
  insecure  = var.ovirt_insecure
}

module "config_ovirt" {
  source                    = "./modules/ovirt/config"

  ovirt_cluster_name        = "Default"
  ovirt_storage_name        = "hosted_storage"
  ovirt_network_name        = "ovirtmgmt"
  ovirt_vnic_profile_name   = "ovirtmgmt"
}


module "template" {
  source                    = "./modules/ovirt/template"

  ovirt_cluster_id          = module.config_ovirt.cluster.id
  ovirt_storage_domain_id   = module.config_ovirt.storagedomain.id
  ovirt_vnic_profile_id     = module.config_ovirt.vnic_profile.id

  template_name             = "mw-ubuntu-18.04"
  template_local_file_path  = abspath("${path.root}/../packer/mw-packer/ubuntu/output-ubuntu-18.04/ubuntu-18.04")
}
